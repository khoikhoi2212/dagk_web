// JavaScript Document
		function Movie(id, name, date, rating, image, length)
		{
			this.id = id;
			this.name = name;
			this.uploadDate = date;
			this.rating = rating;
			this.image = image;
			this.length = length;
			this.actList = [];

			this.addActor = function (newActor) {
        		this.actList.push(newActor);
    		};

    		this.postInfor = function(year, directors, nation, category,description) {
    			this.yearOfManu = year;
    			this.directors = directors;
    			this.nation = nation;
    			this.category = category;
    			this.description = description;
    		}
		}

		function Actor(id, name, image, dayOfBirth, nationality, story)
		{
			this.id = id;
			this.name = name;
			this.image = image;
			this.dayOfBirth = dayOfBirth;
			this.nationality = nationality;
			this.story = story;
			this.movList = [];
		}

		array = [];	
		actorList = [];
		movieList = [];

		array.push(new Movie("mv00001", "Pontianak báo thù", new Date(2019, 9, 24) , 5.9, "assets/img/movie/1.jpg", 92));
		array[0].postInfor("2019", "Glen Goei", "Thái Lan", "Phim kinh dị, Phim tình cảm, Phim lẻ", "Đám cưới tại ngôi làng Malaysia quên mất một vị khách: con ma cà rồng bị khinh miệt quyết tâm thanh toán mối thù bí mật với chú rể và bất cứ ai cản đường cô.");

		array.push(new Movie("mv00002", "Bão trắng 2: trùm á phiện", new Date(2019, 9, 20), 6.2, "assets/img/movie/2.jpg", 99));
		array[1].postInfor("2019", "Herman Yau", "Trung Quốc", "Phim hành động, Phim chiếu rạp, Phim lẻ", "Cùng đầu quân cho Chính Hưng Hội khét tiếng do Yu Nam cầm đầu. Nhưng cuộc đời của Tin và Jizo lại rẽ theo hai hướng đối lập. Trong khi Tin trở thành một tài phiệt, một nhà từ thiện nổi tiếng thì Jizo lại trở thành trùm thuốc phiện của HongKong. Chính Jizo và chân rết của mình đã mang đến cái chết trắng cho người dân Hongkong trong đó có cả vợ sắp cưới của Tin. Phẫn uất trước những tội ác Jizo gây ra, Tin đã treo thưởng 100 triệu đôla cho ai lấy được mạng của Jizo và từ đây, cuộc chiến của những ông trùm bắt đầu...");

		array.push(new Movie("mv00003", "Chuyện kinh dị lúc nửa đêm", new Date(2019, 9, 17), 6.4, "assets/img/movie/3.jpg", 108));
		array[2].postInfor("2019", "André Øvredal", "Mỹ, Canada", "Phim kinh dị, Phim bí ẩn-siêu nhiên, Phim chiếu rạp", "Bối cảnh của “Scary Stories To Tell In The Dark” diễn ra ở nước Mĩ vào năm 1968. Thời thế đang dần xoay chuyển nhưng dường như sự thay đổi ấy chẳng hề ảnh hưởng đến số đông cư dân ở thị trấn Mill Valley. Ở nơi đây, câu chuyện đầy ám ảnh về gia tộc Bellows vẫn được truyền miệng từ thế hệ này đến thế hệ khác. Khởi nguồn của những câu chuyện rùng rợn này bắt đầu từ căn biệt thự nằm ở vùng ven, trong căn nhà này cô bé Sarah Bellows đã phải chịu đựng một cuộc đời đau khổ, bị tra tấn bởi chính người thân trong gia đình. \nSarah biến câu chuyện cuộc đời mình thành tư liệu cho tập truyện kinh dị. Tuy nhiên, bằng một cách kỳ bí nào đó, những câu chuyện ma quái này đã trở thành sự thật với nhóm thiếu niên đã vô tình phát giác ra căn hầm giam giữ Sarah Bellows. Liệu có cách nào để họ có thể thoát khỏi vòng xoáy những câu chuyện mà Sarah đã tạo nên?");

		array.push(new Movie("mv00004", "Công chúa luyện rồng", new Date(2019, 9, 16), 6.1, "assets/img/movie/4.jpg", 75));
		array[3].postInfor("2018", "Marina Nefyodova", "Nga", "Phim viễn tưởng, Phim gia đình, Phim hoạt hình", "Vào sinh nhật 7 tuổi, nàng công chúa Barbara tìm thấy một quyển sách ma thuật có thể đưa cô bé đến Wonderland. Vùng đất thần tiên đó có rồng và những tạo vật kỳ diệu.");

		array.push(new Movie("mv00005", "Chú chó anh hùng", new Date(2019, 9, 13), 6.8, "assets/img/movie/5.jpg", 84));
		array[4].postInfor("2018", "Richard Lanni", "Mỹ", "Phim chiến tranh, Phim phiêu lưu, Phim gia đình, Phim hoạt hình, Phim lẻ", "Sgt. Stubby: An American Hero là câu chuyện về một chú chó dũng cảm và anh hùng. Bộ phim là câu chuyện về một phần lịch sử nước Mỹ qua cái nhìn của một chú chó. Dựa trên câu chuyện thật đã diễn ra trong lịch sử, chú chó Stubby sau này trở thành biểu tượng của Trung đoàn bộ binh trong suốt Thế Chiến thứ nhất, và tham gia vào 17 trận chiến ở mặt trận phía Tây.");

		array.push(new Movie("mv00006", "Giá treo tử thần", new Date(2019, 9, 11), 4.8, "assets/img/movie/6.jpg", 99));
		array[5].postInfor("2019", "Travis Cluff", "Mỹ", "Phim kinh dị, Phim hồi hộp-Gây cấn, Phim Bí ẩn-Siêu nhiên, Phim lẻ", "Tiếp nối phần trước, trong Giá Treo Tử Thần 2, khi Auna Rue chuyển đến trường mới, cô chạm mặt một linh hồn độc ác sau khi tham gia một thử thách lan truyền.");

		array.push(new Movie("mv00007", "Biến thân", new Date(2019, 9, 9), 5.7, "assets/img/movie/7.jpg", 113));
		array[6].postInfor("2019", "Kim Hong-Sun", "Hàn Quốc", "Phim kinh dị, Phim hồi hộp-Gây cấn, Phim lẻ", "Gang Goo và Myung Joo là một cặp vợ chồng, họ có với nhau ba người con: Sun Woo, Hyun Joo và Woo Jong. Sau khi chuyển tới ngôi nhà mới, những điều kỳ lạ cứ liên tiếp xảy đến với gia đình họ. Một con quỷ đã hóa thân thành một thành viên trong gia đình. Sun Woo phải cầu xin người chú là mục sư trừ tà của mình tới giải cứu họ.");

		array.push(new Movie("mv00008", "Quái vật", new Date(2019, 9, 9), 5.7, "assets/img/movie/8.jpg", 130));
		array[7].postInfor("2019", "Lee Jung-Ho", "Hàn Quốc", "Phim hình sự, Phim hồi hộp-Gây cấn, Phim lẻ", "Để có được manh mối quan trọng cho một vụ án giết người thảm khốc, cảnh sát Jeong Han Soo đã phải chấp nhận che giấu một vụ giết người khác. Đối thủ của anh là Han Min Tae đã đánh hơi được việc này và điều tra Jeong Han Soo tới cùng.");

		array.push(new Movie("mv00009", "Tạm biệt mùa hè", new Date(2019, 9, 5), 6.5, "assets/img/movie/9.jpg", 71));
		array[8].postInfor("2019", "Park Ju-Young", "Hàn Quốc", "Phim tâm lý, Phim tình cảm-Lãng mạn, Phim lẻ", "Goodbye Summer là câu chuyện kể về Sumin (Kim Bo Ra), một học sinh ưu tú được Hyun Jae (Jung Jewon) mắc bệnh nan y bày tỏ tình cảm. Vì thời gian sống không còn là bao nên Hyun Jae chọn quay về trường học để trải nghiệm những tháng ngày quý giá cuối cùng trong cuộc đời. Tại đây, giữa một mùa hè nhiệt huyết thanh xuân, hai người đã có những giây phút đáng nhớ và cùng nhau tạo nên những hồi ức đẹp đẽ nhất không bao giờ có thể quên.");

		array.push(new Movie("mv00010", "Giai điệu tình yêu", new Date(2019, 9, 1), 7.3, "assets/img/movie/10.jpg", 122));
		array[9].postInfor("2019", "Jung Ji-Woo", "Hàn Quốc", "Phim chiến tranh, Phim tâm lý, Phim Thể thao-Âm nhạc, Phim lẻ", "Một học sinh và một thiếu niên trầm lặng gặp nhau lần đầu tại một tiệm bánh vào những năm 1990 và cố gắng tìm lại nhau qua nhiều năm, nhưng số phận luôn kéo họ ra xa.\nTrong ngày đầu tiên ca sĩ Yoo Yeol trở thành DJ cho chương trình phát thanh “Music Album”, cũng là ngày đầu tiên Mi Soo gặp Hyun Woo. Những rung động đầu đời chớm nở giữa hai trái tim đầy nhiệt huyết. Nhưng rồi mối tình của họ không kéo dài được lâu, những biến cố liên tục xảy đến khiến họ phải xa nhau. Sau nhiều năm, họ gặp lại và tình cảm này… vẫn chưa kết thúc.");

		array.push(new Movie("mv00011", "Na tra: ma đồng giáng thế", new Date(2019, 8, 29), 7.8, "assets/img/movie/11.jpg", 110));
		array[10].postInfor("2019", "Yu Yang", "Trung Quốc", "Phim viễn tưởng, Phim hồi hộp-Gây cấn, Phim hoạt hình, Phim chiếu rạp, Phim thuyết minh, Phim lẻ", "Từ thuở xa xưa, tinh hoa đất trời hội tụ thành một viên ngọc chứa đựng năng lượng khổng lồ. Nguyên Thủy Thiên Tôn đã phân tách viên ngọc này thành 1 viên Linh Châu và 1 viên Ma Hoàn. Viên Linh Châu sẽ đầu thai thành một anh hùng cứu thế, phò trợ nhà Chu. Trong khi đó, Ma Hoàn sẽ tạo ra một Ma Vương tàn sát thiên hạ. Để ngăn chặn thảm họa, Nguyên Thủy Thiên Tôn đã hạ chú để sau 3 năm Ma Vương sẽ bị Thiên kiếp tiêu diệt. Liệu Na Tra có đủ sức để thay đổi Thiên mệnh?");

		array.push(new Movie("mv00012", "Chuyện chàng kỹ nam", new Date(2019, 8, 27), 6.7, "assets/img/movie/12.jpg", 110));
		array[11].postInfor("2019", "Nam Dae-Joong", "Hàn Quốc", "Phim hài hước, Phim cổ trang, Phim tâm lý, Phim lẻ", "Heo Saek (Lee Joon Ho đóng) là một chàng trai trẻ đẹp trai, mẹ cậu là một kỹ nữ và cậu lớn lên trong kỹ phường. Cậu trở thành kỹ nam đầu tiên ở Joseon, trong khoảng thời gian mà những người phụ nữ bị chèn ép và chịu đựng đau khổ. Trong một lần tình cờ, cậu đã gặp được người con gái khiến cậu trúng tiếng sét ái tình – Hae Won. Hae Won là một tiểu thư xinh đẹp với lối suy nghĩ lạc quan và cởi mở. Nhưng liệu Heo Saek và Hae Won có đến được với nhau khi ngăn cách họ là khác biệt tầng lớp và thân phận?");
		
		array.push(new Movie("mv00013", "Quá nhanh quá nguy hiểm: Hobbs và Shaw", new Date(2019, 8, 24), 6.6, "assets/img/movie/13.jpg", 137));
		array[12].postInfor("2019", "David Leitch", "Mỹ", "Phim hành động, Phim phiêu lưu, Phim chiếu rạp, Phim thuyết minh, Phim lẻ", "Sau 8 phim với phần doanh thu chạm mức 5 tỉ đô la Mỹ toàn cầu, giờ đây thương hiệu Fast & Furious sẽ trở lại với một phần ngoại truyện hoàn toàn độc lập với sự tham gia của Dwayne Johnson trong vai Luke Hobbs và Jason Statham trong vai Deckard Shaw trong Hobbs & Shaw.\nCâu chuyện giữa hai người tưởng như không đội trời chung là Đặc vụ An ninh Ngoại giao Mỹ Luke Hobbs và tên tội phạm đánh thuê khét tiếng Deckard Shaw khi họ bất đắc dĩ phải bắt tay hợp tác nhằm ngăn chặn âm mưu của trùm phản diện cực nguy hiểm trong diện bí ẩn Brixton.");
		
		array.push(new Movie("mv00014", "Rắn đuôi chuông", new Date(2019, 8, 22), 4.3, "assets/img/movie/14.jpg", 85));
		array[13].postInfor("2019", "Zak Hilditch", "Mỹ", "Phim kinh dị, Phim hồi hộp-Gây cấn, Phim Bí ẩn-Siêu nhiên, Phim chính kịch - Drama, Phim lẻ", "Sau khi người phụ nữ bí ẩn cứu con gái cô khỏi vết rắn cắn chết người, bà mẹ đơn thân phải đền món nợ ấy bằng cách giết một người lạ trước khi mặt trời lặn.");
		
		array.push(new Movie("mv00015", "Quỷ dữ rình rập", new Date(2019, 8, 19), 5.3, "assets/img/movie/15.jpg", 89));
		array[14].postInfor("2018", "Quinn Lasher", "Mỹ", "Phim kinh dị, Phim hồi hộp-Gây cấn, Phim chiếu rạp, Phim thuyết minh, Phim lẻ", "Để có một kì nghỉ đáng nhớ, gia đình nhà cô Laura đã chọn một căn nhà bên hồ làm điểm đến. Nhưng đến phút chót, chồng cô vướn phải một cuộc họp khẩn cấp, và Laura quyết định tự đi cùng với hai đứa con gái Maddie và Kayla. Thế nhưng chuyến đi chơi ấy lại dường như hoá thành một cơn ác mộng khi cô và hai đứa con bị tấn công bởi một gã tâm thần - kẻ đã rắp tâm hãm hại gia đình cô từ rất lâu.");
		
		array.push(new Movie("mv00016", "Rudy", new Date(2019, 8, 15), 7.5, "assets/img/movie/16.jpg", 114));	
		array[15].postInfor("1993", "David Anspaugh", "Mỹ", "Phim tài liệu, Phim chính kịch - Drama, Phim Thể thao-Âm nhạc, Phim lẻ", "Dựa trên cậu chuyện có thật về vận động viên bóng bầu dục nổi tiếng Daniel E. Rudy Ruettiger. Anh có đam mê mãnh liệt với bóng bầu dục. Nhưng tất cả mọi người luôn nói rằng anh quá nhỏ bé để có thể chơi bộ môn này. Dù vậy, Rudy vẫn cố vượt qua tất cả để hoàn thành giấc mơ thi đấu cho đội Notre Dame.");

		array.push(new Movie("mv00017", "Đông du", new Date(2019, 8, 13), 6.7, "assets/img/movie/17.jpg", 73));	
		array[16].postInfor("2019", "Đổng Vĩ", "Trung Quốc", "Phim võ thuật, Phim cổ trang, Phim thần thoại, Phim lẻ", "Bộ phim kể về câu chuyện Hàn Diểu kiếp sau của Hàn Tương Tử, một trong Bát Tiên, du ngoạn Đông cung rèn luyện, giúp Đông Hải Long Nữ vượt Long Môn...đã tạo nên một chuyện tình đầy huyền bí");

		array.push(new Movie("mv00018", "Hạnh phúc mong manh", new Date(2019, 8, 10), 6.5, "assets/img/movie/18.jpg", 102));	
		array[17].postInfor("2005", "Gore Verbinski", "Đức", "Phim hài hước, Phim chính kịch - Drama, Phim lẻ", "Dave Spritz là một phát thanh viên về thời tiết trên truyền hình được nhiều người biết đến. Đột nhiên anh được giao cho làm một show truyền hình buổi sáng và tên tuổi của Dave trở nên nổi tiếng trước công chúng. Nhưng gia đình anh thì ngược lại, mối quan hệ của anh và người vợ cũ cùng đứa con trai của mình có nguy cơ tan vỡ. Hòng cứu vãn lại tình thế, Dave đã đến gặp cha của mình, một nhà tiểu thuyết nổi tiếng, tìm kiếm một lời khuyên...");

		array.push(new Movie("mv00019", "Tam đại sư phụ", new Date(2019, 8, 8), 6.2, "assets/img/movie/19.jpg", 97));	
		array[18].postInfor("2007", "Park Sung-Kyun", "Hàn Quốc", "Phim hành động, Phim hài hước, Phim lẻ", "Tại một khu ngoại ô ở Chungcheong , có 2 ông thầy dạy võ cùng họ Kim mở 2 võ quán . Một ông dạy Teakwondo , một ông dạy kiếm đạo . Hai quán trưởng họ Kim này thường xuyên cạnh khóe nhau để tranh dành học trò . Một ông thì nhát gan ham ăn ham ngủ , một ông thì khoái chơi game với con nít . Hai sư phụ Kim ghét nhau chẳng qua là vì họ cùng yêu con gái của ông chủ tiệm mì: nàng Liên Thục giỏi giang và xinh đẹp.\nNhưng giữa lúc cuộc chiến giữa Kim quán trưởng 1 và Kim quán trưởng 2 đang diễn ra gây cấn thì bỗng xuất hiện Kim quán trưởng 3. Người này là một cao thủ Kungfu, vừa đẹp trai hào hoa lại vừa đàn hay hát giỏi. Ông chủ tiệm mì vô cùng khó xử vì con gái thì chỉ có một mà lại có đến ba Kim quán trưởng. Cuối cùng ông quyết định mở một cuộc thi để kén rể...");

		array.push(new Movie("mv00020", "Linh hồn tạm trú", new Date(2019, 8, 5), 7.8, "assets/img/movie/20.jpg", 132));	
		array[19].postInfor("2019", "Parkpoom Wongpoom", "Thái Lan", "Phim viễn tưởng, Phim hồi hộp-Gây cấn, Phim chính kịch - Drama, Phim chiếu rạp, Phim lẻ", "Một linh hồn trú ngụ trong thân xác của Min, chàng nam sinh vừa mới tự sát. Linh hồn này được trao cơ hội tái sinh, với điều kiện trong 100 ngày, anh phải tìm ra nguyên nhân dẫn đến cái chết thảm thương của Min. Nếu không thể hoàn thành, linh hồn của anh sẽ vĩnh viễn không thể siêu thoát. Trong thân xác tạm thời, anh bắt đầu một cuộc sống mới và khai quật những bí mật cũ đang bị chôn vùi.");

		movieList = array;

		actorList.push(new Actor("ac00001", "Nur Fazura", "assets/img/actor/no-image.jpg", "27/09/1983", "Malaysia", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00002", "Remy Ishak", "assets/img/actor/no-image.jpg", "11/04/1982", "Malaysia", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00003", "Hisyam Hamid", "assets/img/actor/no-image.jpg", "20/10/1985", "Singapo", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00004", "Shenty Felizaina", "assets/img/actor/no-image.jpg", "18/08/1989", "Singapo", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00005", "Lưu Đức Hoa", "assets/img/actor/5.jpg", "27/09/1961", "Hongkong", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00006", "Cổ Thiên Lạc", "assets/img/actor/6.jpg", "21/10/1970", "Hongkong", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00007", "Chrissie Chau", "assets/img/actor/7.jpg", "22/05/1985", "Trung Quốc", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00008", "Michelle Wai", "assets/img/actor/8.jpg", "24/11/1984", "Hongkong", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00009", "Kar Yan Lam", "assets/img/actor/9.jpg", "27/09/1983", "Malaysia", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00010", "Kent Cheng", "assets/img/actor/10.jpg", "27/09/1983", "Malaysia", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00011", "Kiu Wai Miu", "assets/img/actor/11.jpg", "27/09/1983", "Malaysia", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00012", "Austin Abrams", "assets/img/actor/12.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00013", "Dean Norris", "assets/img/actor/13.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00014", "Gil Bellows", "assets/img/actor/14.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00015", "Austin Zajur", "assets/img/actor/15.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00016", "Lorraine Toussaint", "assets/img/actor/16.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00017", "Mark Steger", "assets/img/actor/17.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00018", "Ani Lorak", "assets/img/actor/no-image.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00019", "Diomid Vinogradov", "assets/img/actor/no-image.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00020", "Sergey Smirnov", "assets/img/actor/no-image.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00021", "Konstantin Kozhevnikov", "assets/img/actor/no-image.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00022", "Irina Kireeva", "assets/img/actor/no-image.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00023", "Logan Lerman", "assets/img/actor/23.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00024", "Helena Bonham Carter", "assets/img/actor/24.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00025", "Gérard Depardieu", "assets/img/actor/25.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00026", "Pfeifer Brown", "assets/img/actor/26.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00027", "Ema Horvath", "assets/img/actor/no-image.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00028", "Chris Milligan", "assets/img/actor/no-image.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00029", "Brittany Falardeau", "assets/img/actor/no-image.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00030", "Bae Sung-Woo", "assets/img/actor/30.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00031", "Sung Dong-Il", "assets/img/actor/31.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00032", "Jang Young-Nam", "assets/img/actor/32.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00033", "Kim Hye-Jun", "assets/img/actor/33.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00034", "Cho Yi-Hyun", "assets/img/actor/34.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00035", "Lee Sung-Min", "assets/img/actor/35.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00036", "Yoo Jae-Myung", "assets/img/actor/36.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00037", "Jeon Hye-Jin", "assets/img/actor/37.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00038", "Daniel Choi", "assets/img/actor/38.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00039", "An Si-Ha", "assets/img/actor/39.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00040", "One", "assets/img/actor/40.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00041", "Kim Bo-Ra", "assets/img/actor/41.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00042", "Lee Do-Ha", "assets/img/actor/42.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00043", "Lee Gun-Woo", "assets/img/actor/43.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00044", "Shin Young-Kyu", "assets/img/actor/44.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00045", "Choi Ji-Ahn", "assets/img/actor/45.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00046", "Kim Go-Eun", "assets/img/actor/46.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00047", "Jung Hae-In", "assets/img/actor/47.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00048", "Jung Eugene", "assets/img/actor/48.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00049", "Choi Joon-Young", "assets/img/actor/49.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00050", "Park Hae-Joon", "assets/img/actor/50.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00051", "Kim Guk-Hee", "assets/img/actor/51.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00052", "Shim Dal-Gi", "assets/img/actor/52.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00053", "Yanting Lü", "assets/img/actor/no-image.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00054", "Joseph", "assets/img/actor/no-image.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00055", "Mo Han", "assets/img/actor/no-image.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00056", "Hao Chen", "assets/img/actor/no-image.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00057", "Lee Joon-Ho", "assets/img/actor/57.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00058", "Jung So-Min", "assets/img/actor/58.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00059", "Ye Ji-Won", "assets/img/actor/59.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00060", "Choi Gwi-Hwa", "assets/img/actor/60.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00061", "Gong Myung", "assets/img/actor/61.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00062", "Dwayne Johnson", "assets/img/actor/62.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00063", "Jason Statham", "assets/img/actor/63.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00064", "Idris Elba", "assets/img/actor/64.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00065", "Vanessa Kirby", "assets/img/actor/65.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00066", "Helen Mirren", "assets/img/actor/66.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00067", "Eiza González", "assets/img/actor/67.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00068", "Carmen Ejogo", "assets/img/actor/68.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00069", "Theo Rossi", "assets/img/actor/69.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00070", "Emma Greenwell", "assets/img/actor/70.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00071", "Bruce Davis", "assets/img/actor/no-image.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00072", "Yvonne Strahovski", "assets/img/actor/72.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00073", "Abigail Pniowsky", "assets/img/actor/73.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00074", "Anna Pniowsky", "assets/img/actor/no-image.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00075", "Ryan Mcdonald", "assets/img/actor/no-image.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00076", "Sean Astin", "assets/img/actor/76.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00077", "Jon Favreau", "assets/img/actor/77.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00078", "Ned Beatty", "assets/img/actor/78.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00079", "Lili Taylor", "assets/img/actor/79.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00080", "Greta Lind", "assets/img/actor/no-image.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00081", "La Gia Anh", "assets/img/actor/81.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00082", "Trần Bỉnh Cường", "assets/img/actor/no-image.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00083", "Lý Thấm Dao", "assets/img/actor/no-image.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00084", "Trương Viễn", "assets/img/actor/no-image.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00085", "Nicolas Cage", "assets/img/actor/85.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00086", "Michael Caine", "assets/img/actor/86.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00087", "Hope Davis", "assets/img/actor/87.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00088", "Nicholas Hoult", "assets/img/actor/88.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00089", "Michael Rispoli", "assets/img/actor/89.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00091", "Shin Hyun-Joon", "assets/img/actor/91.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00092", "Choi Sung-Guk", "assets/img/actor/92.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00093", "Kwon Oh-Jung", "assets/img/actor/93.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00094", "Oh Seung-Hyun", "assets/img/actor/94.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00095", "Jeong Jun-Ha", "assets/img/actor/95.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00096", "Tak Jae-Hun", "assets/img/actor/96.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00097", "Lee Han-Wi", "assets/img/actor/97.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00098", "Teeradon Supapunpinyo", "assets/img/actor/98.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00099", "Laila Boonyasak", "assets/img/actor/99.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));
		actorList.push(new Actor("ac00100", "Thaneth Warakulnukroh", "assets/img/actor/100.jpg", "Chưa rõ", "Chưa rõ", "Đang cập nhật thông tin"));

		array[0].addActor(actorList[0]);
		array[0].addActor(actorList[1]);
		array[0].addActor(actorList[2]);
		array[0].addActor(actorList[3]);
		array[1].addActor(actorList[4]);
		array[1].addActor(actorList[5]);
		array[1].addActor(actorList[6]);
		array[1].addActor(actorList[7]);
		array[1].addActor(actorList[8]);
		array[1].addActor(actorList[9]);
		array[1].addActor(actorList[10]);
		array[2].addActor(actorList[11]);
		array[2].addActor(actorList[12]);
		array[2].addActor(actorList[13]);
		array[2].addActor(actorList[14]);
		array[2].addActor(actorList[15]);
		array[2].addActor(actorList[16]);
		array[3].addActor(actorList[17]);
		array[3].addActor(actorList[18]);
		array[3].addActor(actorList[19]);
		array[3].addActor(actorList[20]);
		array[3].addActor(actorList[21]);
		array[4].addActor(actorList[22]);
		array[4].addActor(actorList[23]);
		array[4].addActor(actorList[24]);
		array[5].addActor(actorList[25]);
		array[5].addActor(actorList[26]);
		array[5].addActor(actorList[27]);
		array[5].addActor(actorList[28]);
		array[6].addActor(actorList[29]);
		array[6].addActor(actorList[30]);
		array[6].addActor(actorList[31]);
		array[6].addActor(actorList[32]);
		array[6].addActor(actorList[33]);
		array[7].addActor(actorList[34]);
		array[7].addActor(actorList[35]);
		array[7].addActor(actorList[36]);
		array[7].addActor(actorList[37]);
		array[7].addActor(actorList[38]);
		array[8].addActor(actorList[39]);
		array[8].addActor(actorList[40]);
		array[8].addActor(actorList[41]);
		array[8].addActor(actorList[42]);
		array[8].addActor(actorList[43]);
		array[8].addActor(actorList[44]);
		array[9].addActor(actorList[45]);
		array[9].addActor(actorList[46]);
		array[9].addActor(actorList[47]);
		array[9].addActor(actorList[48]);
		array[9].addActor(actorList[49]);
		array[9].addActor(actorList[50]);
		array[9].addActor(actorList[51]);
		array[10].addActor(actorList[52]);
		array[10].addActor(actorList[53]);
		array[10].addActor(actorList[54]);
		array[10].addActor(actorList[55]);
		array[11].addActor(actorList[56]);
		array[11].addActor(actorList[57]);
		array[11].addActor(actorList[58]);
		array[11].addActor(actorList[59]);
		array[11].addActor(actorList[60]);
		array[12].addActor(actorList[61]);
		array[12].addActor(actorList[62]);
		array[12].addActor(actorList[63]);
		array[12].addActor(actorList[64]);
		array[12].addActor(actorList[65]);
		array[12].addActor(actorList[66]);
		array[13].addActor(actorList[67]);
		array[13].addActor(actorList[68]);
		array[13].addActor(actorList[69]);
		array[13].addActor(actorList[70]);
		array[14].addActor(actorList[71]);
		array[14].addActor(actorList[72]);
		array[14].addActor(actorList[73]);
		array[14].addActor(actorList[74]);
		array[15].addActor(actorList[75]);
		array[15].addActor(actorList[76]);
		array[15].addActor(actorList[77]);
		array[15].addActor(actorList[78]);
		array[15].addActor(actorList[79]);
		array[16].addActor(actorList[80]);
		array[16].addActor(actorList[81]);
		array[16].addActor(actorList[82]);
		array[16].addActor(actorList[83]);
		array[17].addActor(actorList[84]);
		array[17].addActor(actorList[85]);
		array[17].addActor(actorList[86]);
		array[17].addActor(actorList[87]);
		array[17].addActor(actorList[88]);
		array[17].addActor(actorList[13]);
		array[18].addActor(actorList[89]);
		array[18].addActor(actorList[90]);
		array[18].addActor(actorList[91]);
		array[18].addActor(actorList[92]);
		array[18].addActor(actorList[93]);
		array[18].addActor(actorList[94]);
		array[18].addActor(actorList[95]);
		array[19].addActor(actorList[96]);
		array[19].addActor(actorList[97]);
		array[19].addActor(actorList[98]);